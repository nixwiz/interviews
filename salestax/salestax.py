#!/usr/bin/env python

import sys
import inflect

# Global scope of non-taxable items
foods = ['chocolate', 'apple', 'bread', 'milk']
medical = ['pills', 'bandage', 'antibiotics']
books = ['book']
# Words to ignore when attempting to match items
ignore = ['of', 'box', 'packet', 'bottle', 'bar']


# This rounding function came from the stackoverflow post:
# https://stackoverflow.com/questions/2272149/round-to-5-or-other-number-in-python
# it was modified with the math.ceil function and updating the inner round to
# two places to force rounding up to the nearest 0.05
def myround(x, prec=2, base=.05):
    import math
    return round(base * math.ceil(round(float(x/base), 2)), prec)


# Need to reorder the appearance of 'imported' in the output such that
# 'box of imported x' becomes 'imported box of x'
def reorder_imported_string(item):
    if item.startswith('imported'):
        return item
    else:
        item_array = item.split()
        item_array.insert(0, item_array.pop(item_array.index('imported')))
        return(' '.join(item_array))


# Determine if specified item is not taxable, by its inclusion in the
# globally defined lists. Use the inflect engine to handle comparing
# singular (e.g. chocolate) with plural (e.g. chocolates)
def not_taxable(item):
    p = inflect.engine()
    for word in item.split():
        if word.lower() in ignore:
            continue
        for nontaxable in foods + medical + books:
            if p.compare(word.lower(), nontaxable.lower()):
                return True
    return False


def process_receipt():
    total_taxes = 0
    grand_total = 0
    line_count = 0

    # Read the receipt from stdin
    for line in sys.stdin.readlines():
        line_count += 1
        try:
            qty = int(line.split()[0])
        except ValueError:
            print("Input line {}, expected integer for quantity, "
                  "found {}".format(line_count, line.split()[0]))
            sys.exit(1)
        try:
            price = float(line.split()[-1])
        except ValueError:
            print("Input line {}, expected price value, "
                  "found {}".format(line_count, line.split()[-1]))
            sys.exit(1)
        item = ' '.join(line.split()[1:-2])

        if not_taxable(item):
            tax = 0.0
        else:
            tax = 0.10

        if "imported" in item:
            tax += .05
            item = reorder_imported_string(item)

        extended_price = qty * price
        taxes = myround(extended_price * tax, 2)
        total_taxes += taxes
        line_total = extended_price + taxes
        grand_total += line_total

        # Print the line item, reformatted
        print("{} {}: {:.2f}".format(qty, item, line_total))

    # Print the tally lines
    print("\nSales Taxes: {0:.2f}".format(total_taxes))
    print("Total: {0:.2f}".format(grand_total))


if __name__ == "__main__":
    process_receipt()
    sys.exit(0)
